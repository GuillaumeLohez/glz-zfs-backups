#!/usr/bin/perl

=head1 general informations

Description:
    Script to make zfs snapshot on localhost

    Config file is : /usr/local/etc/glz_zfs_backups/gzb_make_snapshot.conf

    format is :
    snapshot <local_dataset>

Creation:
    May 2014

Author:
    Guillaume Lohez
    gmp_AT_glz_DOT_io
    http://glz.io

License:
    GPLv2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

=cut


## Standard includes
use strict;
use warnings;
use DateTime;
use LockFile::Simple;
use Log::Handler;
use File::Basename;


## Gzb includes
use Gzb::Common;
use Gzb::Zfs;


# script options
my %options         = ();
$options{'version'} = 'master';
$options{'name'}    = (split(/\./, basename("$0")))[0];
$options{'debug'}   = 0;

$options{'logh'}    = undef;
$options{'log'}     = '/var/log/glz_zfs_backups/'.$options{'name'}.'.log';
$options{'lock'}    = '/var/run/'.$options{'name'};
$options{'common'}  = '/usr/local/etc/glz_zfs_backups/common.conf';
$options{'config'}  = '/usr/local/etc/glz_zfs_backups/'.$options{'name'}.'.conf';


# Check lock ?
my $lockMgr = LockFile::Simple->make(-format => '%f.lock', -stale => 1, -hold => 0);

if(not $lockMgr->trylock($options{'lock'})) {

    die($options{'name'}.' is already running !?!?');
}


# General log
%options = create_log_handler(\%options);
if(not defined($options{'logh'})) {

	die('Can not write log: '.$options{'log'}."\n");
}

# Parameters from file
my %parameters = read_config_files(\%options);


# Put a tag in general log
$options{'logh'}->info("Begin job");

# Go accross hashtable and do the jobs !
foreach my $dataset (@{$parameters{'snapshot'}}) {

	$options{'logh'}->info("Working on $dataset");

	my $lastSnapshot        = undef;
    my $lastSnapshotDate    = undef;
    my $lastSnapshotDt      = undef;

	# Find last snapshot
	if(check_if_dataset_exist(\%options, \%parameters, $dataset)) {

		write_debug_if_requested(\%options, "dataset: $dataset present");

		$lastSnapshot = get_last_snapshot(\%options, \%parameters, $dataset);

        if(defined($lastSnapshot)) {

            write_debug_if_requested(\%options, "last snapshot is : $lastSnapshot");

            $lastSnapshotDate    = (split(/@/, $lastSnapshot))[1];
            my ($lastSnapshotYear, $lastSnapshotMonth, $lastSnapshotDay) = unpack("A4A2A2", $lastSnapshotDate);
            $lastSnapshotDt         = DateTime->new(
                year        => $lastSnapshotYear,
                month       => $lastSnapshotMonth,
                day         => $lastSnapshotDay,
                time_zone   => $parameters{'timezone'},
            );
        }
        else {
            $options{'logh'}->warning("Can not find last snapshot for dataset: $dataset !");
        }
	}
	else {

		$options{'logh'}->error("local dataset: $dataset does not exist !");
	}

	my $todayDt = DateTime->today(time_zone => $parameters{'timezone'});
	my $today   = $todayDt->ymd('');
    my $doing   = 0;

	if(defined($lastSnapshotDt) and $lastSnapshotDt == $todayDt)	{

		$options{'logh'}->info("No need to snapshot: $dataset, $lastSnapshotDate already present");
	}
	elsif(defined($lastSnapshotDt) and $lastSnapshotDt < $todayDt)	{

        $doing = 1;
    }
    else {

        $doing = 1;
    }

    if($doing) {

		$options{'logh'}->info("Need to snapshot: $dataset");
		my $exitCode = make_snapshot(\%options, \%parameters, $dataset, $today);

		if($exitCode) {

			$options{'logh'}->info("Successfully snapshot $dataset with date: $today");
		}
		else {

			$options{'logh'}->error("Problem happened during snapshot of $dataset");
		}
	}
}

# Put a tag in general log
$options{'logh'}->info("Job end");

# unlock
$lockMgr->unlock($options{'lock'});

