#!/usr/bin/perl

=head1 general informations

Description:
    Script to backup via rsync a directory to zfs dataset and snapshot it

    Config file is : /usr/local/etc/glz_zfs_backups/gzb_backup_directory.conf

    format is :
    backup <server>:<dir> = <local_dataset>

Creation:
    May 2014

Author:
    Guillaume Lohez
    gmp_AT_glz_DOT_io
    http://glz.io

License:
    GPLv2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

=cut


## Standard includes
use strict;
use warnings;
use DateTime;
use LockFile::Simple;
use Log::Handler;
use File::Basename;


## Gzb includes
use Gzb::Common;
use Gzb::Zfs;
use Gzb::Rsync;


# script options
my %options         = ();
$options{'version'} = 'master';
$options{'name'}    = (split(/\./, basename("$0")))[0];
$options{'debug'}   = 0;

$options{'logh'}    = undef;
$options{'log'}     = '/var/log/glz_zfs_backups/'.$options{'name'}.'.log';
$options{'lock'}    = '/var/run/'.$options{'name'};
$options{'common'}  = '/usr/local/etc/glz_zfs_backups/common.conf';
$options{'config'}  = '/usr/local/etc/glz_zfs_backups/'.$options{'name'}.'.conf';


# Check lock ?
my $lockMgr = LockFile::Simple->make(-format => '%f.lock', -stale => 1, -hold => 0);

if(not $lockMgr->trylock($options{'lock'})) {

    die($options{'name'}.' is already running !?!?');
}


# General log
%options = create_log_handler(\%options);
if(not defined($options{'logh'})) {

    die('Can not write log: '.$options{'log'});
}

# Parameters from file
my %parameters = read_config_files(\%options);


# Put a tag in general log
$options{'logh'}->info("Job begin");


# Go accross hashtable and do the jobs !
while(my($src, $dst)=each(%{$parameters{'backup'}})) {

    my @tmp=split(/:/, $src);
	my $srv=$tmp[0];
	my $dir=$tmp[1];

    $options{'logh'}->info("backup of: $dir from: $srv to dataset: $dst");

    if(check_if_dataset_exist(\%options, \%parameters, $dst)) {

        write_debug_if_requested(\%options, "dataset: $dst present");
		
		# Get mountpoint
		my $dstDir = get_mountpoint(\%options, \%parameters, $dst);

		# copy datas
		my $exitCodeRsync=undef;
        my $wan = 0;
		if($srv !~ /^\w+\.int\.\w+/ and $srv !~ /^\w+\.dmz\.\w+/) {
            $wan = 1;
		}

        $exitCodeRsync = rsync_folder(\%options, \%parameters, $srv, $dir, $dstDir, $wan);
		if($exitCodeRsync) {

			$options{'logh'}->info("Successfully copy directory: $src to dataset $dst");
		}
		else {

			$options{'logh'}->error("Problem happened during copy of $src to dataset $dst");
		}

		# Snapshot dataset
		my $todayDt = DateTime->today( time_zone => $parameters{'timezone'} );
		my $today   = $todayDt->ymd('');
		my $exitCodeSnapshot = make_snapshot(\%options, \%parameters, $dst, $today);		
        if($exitCodeSnapshot) {

            $options{'logh'}->info("Successfully snapshot $dst with date: $today");
        }
        else {

            $options{'logh'}->error("Problem happened during snapshot of $dst");
        }

    }
    else {

        $options{'logh'}->error("local dataset: $dst does not exist !");
    }
}

# Put a tag in general log
$options{'logh'}->info("Job end");

# unlock
$lockMgr->unlock($options{'lock'});

