#!/usr/bin/perl

=head1 general informations

Description:
    Script to zfs snapshot to another zfs server

    Config file is : /usr/local/etc/glz_zfs_backups/gzb_send_snapshot_to_remote.conf

    format is :
    send <local_dataset> = <remote_server:dataset>

Creation:
    May 2014

Author:
    Guillaume Lohez
    gmp_AT_glz_DOT_io
    http://glz.io

License:
    GPLv2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

=cut


## Standard includes
use strict;
use warnings;
use DateTime;
use LockFile::Simple;
use Log::Handler;
use File::Basename;


## Gzb includes
use Gzb::Common;
use Gzb::Zfs;


# script options
my %options         = ();
$options{'version'} = 'master';
$options{'name'}    = (split(/\./, basename("$0")))[0];
$options{'debug'}   = 0;

$options{'logh'}    = undef;
$options{'log'}     = '/var/log/glz_zfs_backups/'.$options{'name'}.'.log';
$options{'lock'}    = '/var/run/'.$options{'name'};
$options{'common'}  = '/usr/local/etc/glz_zfs_backups/common.conf';
$options{'config'}  = '/usr/local/etc/glz_zfs_backups/'.$options{'name'}.'.conf';


# Check lock ?
my $lockMgr = LockFile::Simple->make(-format => '%f.lock', -stale => 1, -hold => 0);

if(not $lockMgr->trylock($options{'lock'})) {

    die($options{'name'}.' is already running !?!?');
}


# General log
%options = create_log_handler(\%options);
if(not defined($options{'logh'})) {

    die('Can not write log: '.$options{'log'});
}

# Parameters from file
my %parameters = read_config_files(\%options);


# Put a tag in general log
$options{'logh'}->info("Job begin");


# Go accross hashtable and do the jobs !
while(my($dataset, $serverDataset)=each(%{$parameters{'send'}})) {

	$options{'logh'}->info("Working on $dataset to send to $serverDataset");

	my $lastSnapshot=undef;
	my $lastRemoteSnapshot=undef;
	
	# Find last local snapshot
	if(check_if_dataset_exist(\%options, \%parameters, $dataset)) {

        write_debug_if_requested(\%options, "dataset: $dataset present");

		$lastSnapshot = get_last_snapshot(\%options, \%parameters, $dataset);
        write_debug_if_requested(\%options, "last local snapshot is : $lastSnapshot");
	}
	else {

		$options{'logh'}->error("local dataset: $dataset does not exist !");
	}
	if(!defined($lastSnapshot) or $lastSnapshot !~ /$dataset\@\d+/) {

		$options{'logh'}->error("Can not find last local snapshot for dataset: $dataset !");
		next;
	}

	my $lastSnapshotDate    = (split(/@/, $lastSnapshot))[1];
	my ($lastSnapshotYear, $lastSnapshotMonth, $lastSnapshotDay) = unpack("A4A2A2", $lastSnapshotDate);
	my $lastSnapshotDt      = DateTime->new(
        year        => $lastSnapshotYear,
        month       => $lastSnapshotMonth,
        day         => $lastSnapshotDay,
        time_zone   => $parameters{'timezone'},
    );


	# Extract server and remote dataset
    my @tmp             = split(/:/, $serverDataset);
	my $server          = $tmp[0];
	my $remoteDataset   = $tmp[1];

	if(not defined($server) or not defined($remoteDataset)) {

		$options{'logh'}->error("Can not understand parameter server:remote_dataset : $serverDataset !");
		next;
	}

	# Find last remote snapshot
	if(check_if_dataset_exist(\%options, \%parameters, $remoteDataset, $server)) {

        write_debug_if_requested(\%options, "dataset: $remoteDataset on server: $server present");

		$lastRemoteSnapshot = get_last_snapshot(\%options, \%parameters, $remoteDataset, $server);
        if(defined($lastRemoteSnapshot)) {
            write_debug_if_requested(\%options, "last remote snapshot on server: $server is : $lastRemoteSnapshot");
        }
	}
	else {

		$options{'logh'}->error("remote dataset: $remoteDataset does not exist on server: $server !");
	}
	if(defined($lastRemoteSnapshot) and $lastRemoteSnapshot =~ /$remoteDataset\@\d+/) {

        # we have a remote snaptshot, use incremental send
        my $lastRemoteSnapshotDate  = (split(/@/, $lastRemoteSnapshot))[1];
        my ($lastRemoteSnapshotYear, $lastRemoteSnapshotMonth, $lastRemoteSnapshotDay) = unpack("A4A2A2", $lastRemoteSnapshotDate);
        my $lastRemoteSnapshotDt    = DateTime->new(
            year        => $lastRemoteSnapshotYear,
            month       => $lastRemoteSnapshotMonth,
            day         => $lastRemoteSnapshotDay,
            time_zone   => $parameters{'timezone'},
        );
        
        if($lastSnapshotDt == $lastRemoteSnapshotDt) {

            $options{'logh'}->info("No need to send snapshot: $lastSnapshot to server: $server, already present");
        }
        elsif($lastRemoteSnapshotDt < $lastSnapshotDt) {

            $options{'logh'}->info("Need to send incremental snapshot: $lastSnapshot to server: $server");
            my $exitCode = send_incremental_snapshot(\%options, \%parameters, $dataset, $lastSnapshotDate, $lastRemoteSnapshotDate, $server, $remoteDataset);

            if($exitCode) {

                $options{'logh'}->info("Successfully sent incremental $lastSnapshot from : $lastRemoteSnapshotDate to server: $server");
            }
            else {

                $options{'logh'}->error("Problem happened during transfer of $lastSnapshot to server: $server");
            }
        }
    }
    else {

        # no remote snapshot, use full send
        $options{'logh'}->info("Need to send full snapshot: $lastSnapshot to server: $server");
        my $exitCode = send_snapshot(\%options, \%parameters, $dataset, $lastSnapshotDate, $server, $remoteDataset);

        if($exitCode) {

            $options{'logh'}->info("Successfully sent full $lastSnapshot to server: $server");
        }
        else {

            $options{'logh'}->error("Problem happened during transfer of $lastSnapshot to server: $server");
        }
	}
}

# Put a tag in general log
$options{'logh'}->info("Job end");

# unlock
$lockMgr->unlock($options{'lock'});

