

=head1 general informations

Description:
    This package contains all zfs functions for glz-zfs-backups a.k.a. gzb

Creation:
    May 2014

Author:
    Guillaume Lohez
    gzb_AT_glz_DOT_io
    http://glz.io

License:
    GPLv2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

=cut


## Name of this package
package Gzb::Zfs;


## Standard includes
use strict;
use warnings;
require Exporter;
use File::Basename;


## Gzb includes
use Gzb::Common;


## Exports
our $VERSION    = 'master';
our @ISA        = qw(Exporter);
our @EXPORT     = qw(
    check_if_dataset_exist
    get_last_snapshot
    get_all_snapshots
    make_snapshot
    destroy_snapshot
    send_incremental_snapshot
    get_mountpoint
    send_snapshot
);



=head1 check_if_dataset_exist()

Check if dataset exist or not
Parameters:
    - hash table of options
    - hash table of parameters
    - dataset name
    - server name

check_if_dataset_exist(\%options, \%parameters, $dataset_name, $server_name)

if server name is localhost or not defined, use zfs directly, do not connect by ssh

=cut

sub check_if_dataset_exist {

    my ($ref_options, $ref_parameters, $ds, $server) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    my $exist = undef;

    my $zfs_list = undef;
    if(not defined($server) or $server eq 'localhost') {

        $zfs_list = $parameters{'zfs_cmd'}." list $ds >/dev/null 2>&1";
    }
    else {

        $zfs_list = $parameters{'ssh_cmd'}.' -i '.$parameters{'ssh_key'}.' '.$parameters{'ssh_login'}."\@$server \"".$parameters{'zfs_cmd'}." list $ds >/dev/null 2>&1\"";
    }
    write_debug_if_requested(\%options, "zfs list command: $zfs_list");
    `$zfs_list`;

    if($? == 0) {

        $exist = 1;
    }
    else {

        $exist = 0;
    }    
    return $exist;
}

=pod

Return true for success

=cut



=head1 get_last_snapshot()

Get name of last local snapshot
Parameters:
    - hash table of options
    - hash table of parameters
    - dataset name
    - server name

get_last_snapshot(\%options, \%parameters, $dataset_name, $server_name)

=cut

sub get_last_snapshot {

    my ($ref_options, $ref_parameters, $ds, $server) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    my $snapshot=undef;

    my @snapshots = &get_all_snapshots(\%options, \%parameters,  $ds, $server);
    if(defined($snapshots[0]))
    {

        $snapshot = $snapshots[0];
    }
    
    return $snapshot;
}

=pod

Return snapshot name in case of success
Or return undef

=cut


=head1 get_all_snapshots()

List all snapshots
Parameters:
    - hash table of options
    - hash table of parameters
    - dataset name
    - remote server

get_all_local_snapshots(\%options, \%parameters, $dataset_name, $remote_server)

if server name is localhost or not defined, use zfs directly, do not connect by ssh

=cut

sub get_all_snapshots {

    my ($ref_options, $ref_parameters, $ds, $server) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    my @snapshots = ();

    my $zfs_list=undef;
    if(not defined($server) or $server eq 'localhost') {

        $zfs_list = $parameters{'zfs_cmd'}." list -t snapshot -r -H -o name -S name $ds 2>&1";
    }
    else {

        $zfs_list = $parameters{'ssh_cmd'}.' -i '.$parameters{'ssh_key'}.' '.$parameters{'ssh_login'}."\@$server \"".$parameters{'zfs_cmd'}." list -t snapshot -r -H -o name -S name $ds 2>&1\"";
    }
    write_debug_if_requested(\%options, "zfs list command: $zfs_list");
    my @tmp = `$zfs_list`;

    foreach my $snap (@tmp) {

        chomp($snap);
        push(@snapshots, $snap);
    }

    return @snapshots;
}

=pod

Return an array with all snapshots
Array is empty if no snapshot found

=cut



=head1 make_snapshot()

Make snapshot
Parameters:
    - hash table of options
    - hash table of parameters
    - hash table of parameters
    - dataset name
    - snapshot name

make_snapshot(\%options, \%parameters, $dataset_name, $snapshot_name)

=cut

sub make_snapshot {

    my ($ref_options, $ref_parameters, $ds, $name) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    my $done = undef;

    my $zfs_snap = $parameters{'zfs_cmd'}." snapshot $ds\@$name >/dev/null 2>&1";
    write_debug_if_requested(\%options, "zfs snapshot command: $zfs_snap");
    `$zfs_snap`;

    if($? == 0) {

        $done = 1;
    }
    else {

        $done = 0;
    }
    
    return $done;
}

=pod

Return true for success

=cut



=head1 destroy_snapshot()

Destroy snapshot
Parameters:
    - hash table of options
    - hash table of parameters
    - snapshot name

make_snapshot(\%options, \%parameters, $snapshot_name)

=cut

sub destroy_snapshot {

    my ($ref_options, $ref_parameters, $name) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    my $done = undef;

    my $zfs_destroy = $parameters{'zfs_cmd'}." destroy $name >/dev/null 2>&1";
    write_debug_if_requested(\%options, "zfs destroy command: $zfs_destroy");
    `$zfs_destroy`;

    if($? == 0) {

        $done=1;
    }
    else {

        $done=0;
    }
    
    return $done;
}

=pod

Return true for success

=cut



=head1 send_incremental_snapshot()

Send incremental snapshot to a remote server
Parameters:
    - hash table of options
    - hash table of parameters
    - dataset name
    - last snap date
    - remote last snap date
    - remote server
    - remote dataset

send_incremental_snapshot(\%options, \%parameters, $dataset_name, $last_snam_date, $remote_last_snap_date, $remote_server, $remote_dataset)

=cut

sub send_incremental_snapshot {

    my ($ref_options, $ref_parameters, $ds, $ls, $rls, $server, $rds) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    my $done = undef;

    my $zfs_send = $parameters{'zfs_cmd'}." send -i $ds\@$rls $ds\@$ls | ".$parameters{'ssh_cmd'}.' -i '.$parameters{'ssh_key'}.' '.$parameters{'ssh_login'}."\@$server \"".$parameters{'zfs_cmd'}." receive -F $rds >/dev/null 2>&1\"";
    write_debug_if_requested(\%options, "zfs send/receive command: $zfs_send");
    `$zfs_send`;

    if($? == 0) {

        $done = 1;
    }
    else {

        $done = 0;
    }
    
    return $done;
}

=pod

Return true for success

=cut



=head1 get_mountpoint()

Get mountpoint of a dataset
Parameters:
    - hash table of options
    - hash table of parameters
    - dataset name

get_mountpoint(\%options, \%parameters, $dataset_name)


=cut

sub get_mountpoint {

    my ($ref_options, $ref_parameters, $ds) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    my $mp = undef;

    my $zfs_mountpoint = $parameters{'zfs_cmd'}." get -o value -H mountpoint $ds";
    write_debug_if_requested(\%options, "zfs get mountpoint command: $zfs_mountpoint");
    $mp = `$zfs_mountpoint`;
    chomp($mp);

    if(not $mp) {

        $mp = undef;
    }
    else {

        return $mp;
    }
    return $mp;
}

=pod

Return mountpoint or undef if mountpoint not found

=cut



=head1 send_snapshot()

Send snapshot to a remote server
Parameters:
    - hash table of options
    - hash table of parameters
    - dataset name
    - last snap date
    - remote server
    - remote dataset

send_snapshot(\%options, \%parameters, $dataset_name, $last_snam_date, $remote_server, $remote_dataset)

=cut

sub send_snapshot {

    my ($ref_options, $ref_parameters, $ds, $ls, $server, $rds) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    my $done = undef;

    my $zfs_send = $parameters{'zfs_cmd'}." send $ds\@$ls | ".$parameters{'ssh_cmd'}.' -i '.$parameters{'ssh_key'}.' '.$parameters{'ssh_login'}."\@$server \"".$parameters{'zfs_cmd'}." receive -F $rds >/dev/null 2>&1\"";
    write_debug_if_requested(\%options, "zfs send/receive command: $zfs_send");
    `$zfs_send`;

    if($? == 0) {

        $done = 1;
    }
    else {

        $done = 0;
    }
    
    return $done;
}

=pod

Return true for success

=cut



1;

