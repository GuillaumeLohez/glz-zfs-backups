

=head1 general informations

Description:
    This package contains all common functions for glz-zfs-backups a.k.a. gzb

Creation:
    May 2014

Author:
    Guillaume Lohez
    gzb_AT_glz_DOT_io
    http://glz.io

License:
    GPLv2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

=cut


## Name of this package
package Gzb::Common;


## Standard includes
use strict;
use warnings;
require Exporter;
use Log::Handler;
use AppConfig qw(:argcount);
use File::Basename;


## Exports
our $VERSION    = 'master';
our @ISA        = qw(Exporter);
our @EXPORT     = qw(
    write_debug_if_requested
    read_config_files
    create_log_handler
);



=head1 write_debug_if_requested()

Write debug information if $options{'debug'} is true
Parameters:
    - hash table of options
    - message

write_debug_if_requested(\%options, $message);

=cut

sub write_debug_if_requested {

    my($ref_options, $message) = @_;
    my %options = %{$ref_options};

    if($options{'debug'}) {

        $options{'logh'}->debug("$message");
    }
}

=pod

Return nothing... This is just a print

=cut



=head1 read_config_files()

Get values from configurations files
Parameters:
    - hash table of options

read_config_files(\%options);

=cut

sub read_config_files {

    my ($ref_options) = @_;
    my %options     = %{$ref_options};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    my %parameters = ();

    my $config  = AppConfig->new();

    # Common
    $config->define('zfs_cmd', {
        DEFAULT     => '/sbin/zfs',
        ARGCOUNT    => ARGCOUNT_ONE,
    });
    $config->define('ssh_cmd', {
        DEFAULT     => '/usr/bin/ssh',
        ARGCOUNT    => ARGCOUNT_ONE,
    });
    $config->define('ssh_key', {
        DEFAULT     => '/root/.ssh/id_rsa',
        ARGCOUNT    => ARGCOUNT_ONE,
    });
    $config->define('ssh_login', {
        DEFAULT     => 'root',
        ARGCOUNT    => ARGCOUNT_ONE,
    });
    $config->define('rsync_cmd', {
        DEFAULT     => '/usr/local/bin/rsync',
        ARGCOUNT    => ARGCOUNT_ONE,
    });
    $config->define('rsync_options', {
        DEFAULT     => '--archive --delete --partial --exclude=\"*lost+found*\" --exclude=\"*.zfs*\"',
        ARGCOUNT    => ARGCOUNT_ONE,
    });
    $config->define('rsync_lan_max_speed', {
        DEFAULT     => '1000000',
        ARGCOUNT    => ARGCOUNT_ONE,
    });
    $config->define('rsync_wan_max_speed', {
        DEFAULT     => '1000',
        ARGCOUNT    => ARGCOUNT_ONE,
    });
    $config->define('timezone', {
        DEFAULT     => 'Europe/Paris',
        ARGCOUNT    => ARGCOUNT_ONE,
    });


    # read common config file
    if(-e $options{'common'}) {

        $options{'logh'}->info('Read config from file: '.$options{'common'});
        $config->file($options{'common'});
    }

    $parameters{'zfs_cmd'}              = $config->zfs_cmd();
    $parameters{'ssh_cmd'}              = $config->ssh_cmd();
    $parameters{'ssh_key'}              = $config->ssh_key();
    $parameters{'ssh_login'}            = $config->ssh_login();
    $parameters{'rsync_cmd'}            = $config->rsync_cmd();
    $parameters{'rsync_options'}        = $config->rsync_options();
    $parameters{'rsync_lan_max_speed'}  = $config->rsync_lan_max_speed();
    $parameters{'rsync_wan_max_speed'}  = $config->rsync_wan_max_speed();
    $parameters{'timezone'}             = $config->timezone();

    # for gzb_make_snapshot
    $config->define('snapshot', {
        ARGCOUNT    => ARGCOUNT_LIST,
    });

    # for gzb_arrange_snapshot
    $config->define('arrange', {
        ARGCOUNT    => ARGCOUNT_LIST,
    });

    # for gzb_backup_directory
    $config->define('backup', {
        ARGCOUNT    => ARGCOUNT_HASH,
    });

    # for gzb_backup_server
    $config->define('server', {
        ARGCOUNT    => ARGCOUNT_HASH,
    });

    # for gzb_clean_old_snapshots
    $config->define('clean', {
        ARGCOUNT    => ARGCOUNT_HASH,
    });

    # for gzb_send_snapshot_to_remote
    $config->define('send', {
        ARGCOUNT    => ARGCOUNT_HASH,
    });

    # read specific config file
    if(not -e $options{'config'}) {

        $options{'logh'}->error('Config file: '.$options{'config'}.' does not exist !?!?');
        exit(1);
    }

    $options{'logh'}->info('Read config from file: '.$options{'config'});
    $config->file($options{'config'});

    # for gzb_make_snapshot
    foreach my $snapshot (@{$config->snapshot()}) {
        push (@{$parameters{'snapshot'}}, $snapshot);
    }

    # for gzb_arrange_snapshot
    foreach my $arrange (@{$config->arrange()}) {
        push (@{$parameters{'arrange'}}, $arrange);
    }

    # for gzb_backup_directory
    while(my($src, $dst) = each(%{$config->backup()})) {
        $parameters{'backup'}{"$src"} = $dst;
    }

    # for gzb_backup_server
    while(my($src, $dst) = each(%{$config->server()})) {
        $parameters{'server'}{"$src"} = $dst;
    }

    # for gzb_clean_old_snapshots
    while(my($dataset, $keep) = each(%{$config->clean()})) {
        $parameters{'clean'}{"$dataset"} = $keep;
    }

    # for gzb_send_snapshot_to_remote
    while(my($dataset, $dst) = each(%{$config->send()})) {
        $parameters{'send'}{"$dataset"} = $dst;
    }

    return %parameters
}

=pod

Return hash table with real values

=cut



=head1 create_log_handler()

Create log handler for all log
Parameters:
    - hash table of options

read_config_files(\%options);

=cut

sub create_log_handler {

    my ($ref_options) = @_;
    my %options     = %{$ref_options};

    if(not -w dirname($options{'log'})) {

        $options{'logh'} = undef;
    }
    else {
        my $log = Log::Handler->new();

        $log->add(
            file => {
                timeformat      => '%Y/%m/%d %H:%M:%S',
                message_layout  => '[%T] [%P] [%L] %m',
                filename        => $options{'log'},
                maxlevel        => 7,
                minlevel        => 0
            }
        );
        $log->add(
            screen => {
                log_to   => 'STDERR',
                maxlevel => 3,
                minlevel => 0,
            }
        );

        # put log handler in config for debug write
        $options{'logh'} = $log;

        # write at the end of the log_handler does not exists !
        write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);
    }

    return %options
}

=pod

Return true if everything is ok

=cut



1;
