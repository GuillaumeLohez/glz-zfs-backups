

=head1 general informations

Description:
    This package contains all rsync functions for glz-zfs-backups a.k.a. gzb

Creation:
    May 2014

Author:
    Guillaume Lohez
    gzb_AT_glz_DOT_io
    http://glz.io

License:
    GPLv2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

=cut


## Name of this package
package Gzb::Rsync;


## Standard includes
use strict;
use warnings;
require Exporter;
use AppConfig;


## Gzb includes
use Gzb::Common;


## Exports
our $VERSION    = 'master';
our @ISA        = qw(Exporter);
our @EXPORT     = qw(
    rsync_folder
);



=head1 rsync_folder()

rsync directory
Parameters:
    - hash table of options
    - hash table of parameters
    - remote server
    - remote dir
    - local dir
    - wan, if true, slow down and compress

rsync_folder(\%options, \%parameters, $remote_server, $remote_dir, $local_dir, $wan)

=cut

sub rsync_folder {
    
    my ($ref_options, $ref_parameters, $srv, $src, $dst, $wan) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    my $exitCode = undef;

    # write timestamp before copy
    my $write_timestamp = $parameters{'ssh_cmd'}.' -i '.$parameters{'ssh_key'}." $srv \"/bin/date >$src/.timestamp\"";
    write_debug_if_requested(\%options, "write timestamp command: $write_timestamp");
    `$write_timestamp`;

    # copy datas
    my $rsync_full_options = undef;
    if($wan) {
        $rsync_full_options = $parameters{'rsync_options'}.' --compress --bwlimit='.$parameters{'rsync_wan_max_speed'}; 
    }
    else {
        $rsync_full_options = $parameters{'rsync_options'}.' --bwlimit='.$parameters{'rsync_lan_max_speed'}; 
    }
    my $copy_datas = $parameters{'rsync_cmd'}." -e \"".$parameters{'ssh_cmd'}.' -i '.$parameters{'ssh_key'}."\" $rsync_full_options $srv:$src/ $dst/ >/dev/null 2>&1";
    write_debug_if_requested(\%options, "copy data command: $copy_datas");
    `$copy_datas`;

    # check return code
    if($? == 0) {

        $exitCode = 1;
    }
    else {

        $exitCode = 0;
    }

    return $exitCode;

}

=pod

Return true for success

=cut



1;
