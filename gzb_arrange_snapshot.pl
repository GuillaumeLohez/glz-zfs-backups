#!/usr/bin/perl

=head1 general informations

Description:
    Script to arrange zfs snapshot on localhost
    Keep every first day of month snapshots
    Then every mondays snapshots (but not the first as it near of the first day of the month)
    And keep last five days snapshots

    Config file is : /usr/local/etc/glz_zfs_backups/gzb_arrange_snapshot.conf

    format is :
    arrange <local_dataset>

Creation:
    May 2014

Author:
    Guillaume Lohez
    gmp_AT_glz_DOT_io
    http://glz.io

License:
    GPLv2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

=cut


## Standard includes
use strict;
use warnings;
use DateTime;
use LockFile::Simple;
use Log::Handler;
use File::Basename;


## Gzb includes
use Gzb::Common;
use Gzb::Zfs;


# script options
my %options         = ();
$options{'version'} = 'master';
$options{'name'}    = (split(/\./, basename("$0")))[0];
$options{'debug'}   = 0;

$options{'logh'}    = undef;
$options{'log'}     = '/var/log/glz_zfs_backups/'.$options{'name'}.'.log';
$options{'lock'}    = '/var/run/'.$options{'name'};
$options{'common'}  = '/usr/local/etc/glz_zfs_backups/common.conf';
$options{'config'}  = '/usr/local/etc/glz_zfs_backups/'.$options{'name'}.'.conf';


# Check lock ?
my $lockMgr = LockFile::Simple->make(-format => '%f.lock', -stale => 1, -hold => 0);

if(not $lockMgr->trylock($options{'lock'})) {

    die($options{'name'}.' is already running !?!?');
}


# General log
%options = create_log_handler(\%options);
if(not defined($options{'logh'})) {

    die('Can not write log: '.$options{'log'}."\n");
}

# Parameters from file
my %parameters = read_config_files(\%options);


# Put a tag in general log
$options{'logh'}->info("Begin job");


# Build last snapshot to keep date
my $numberDayLastSnapshot   = 5;
my $lastSnapToKeepDt        = DateTime->today();
$lastSnapToKeepDt->subtract(days => $numberDayLastSnapshot);

# Go accross hashtable and do the jobs !
foreach my $dataset (@{$parameters{'arrange'}}) {

    $options{'logh'}->info("Working on $dataset");

    if(check_if_dataset_exist(\%options, \%parameters, $dataset)) {

        write_debug_if_requested(\%options, "dataset: $dataset present");

        my @allSnapshots=get_all_snapshots(\%options, \%parameters, $dataset);
        foreach my $snapshot (@allSnapshots) {

            write_debug_if_requested(\%options, "Current snapshot: $snapshot");

			my $snapshotDate = (split(/@/, $snapshot))[1];
			my ($snapshotYear, $snapshotMonth, $snapshotDay) = unpack("A4A2A2", $snapshotDate);
			my $snapshotDt = DateTime->new(
                year        => $snapshotYear,
                month       => $snapshotMonth,
                day         => $snapshotDay,
                time_zone   => $parameters{'timezone'},
            );

			if($snapshotDt >= $lastSnapToKeepDt) {

                write_debug_if_requested(\%options, "Keep this snapshot, less then $numberDayLastSnapshot day(s)");
			}
			else {

                write_debug_if_requested(\%options, "We are after the last numberLastSnapshot snapshots");

                # If we are not the 1st
                # and if we are monday in the first 5 days of the month
                #   or we are not monday
                # -> drop the snapshot
				if( ($snapshotDt->day_of_week != 1 or ($snapshotDt->day_of_week == 1 and $snapshotDay < 5))
                    and $snapshotDate !~ /20\d{4}01/) {

					$options{'logh'}->info("Need to delete snapshot: $snapshotDate");
					my $exitCode = destroy_snapshot(\%options, \%parameters, $snapshot);

					if($exitCode) {

						$options{'logh'}->info("Successfully delete snapshot $snapshot");
					}
					else {

						$options{'logh'}->error("Problem happened during delete snapshot: $snapshot");
					}
				}
				else {

                    write_debug_if_requested(\%options, "Keep this snapshot");
				}
			}
        }
    }
    else {

        $options{'logh'}->error("local dataset: $dataset does not exist !");
    }
}

# Put a tag in general log
$options{'logh'}->info("Job end");

# unlock
$lockMgr->unlock($options{'lock'});

