#!/usr/bin/perl

=head1 general informations

Description:
    Script to clean old zfs snapshot on localhost sorted by date reverse

    Config file is : /usr/local/etc/glz_zfs_backups/gzb_clean_old_snapshot.conf

    format is :
    clean <local_dataset> = <number_of_snapshot_to_keep>

Creation:
    May 2014

Author:
    Guillaume Lohez
    gmp_AT_glz_DOT_io
    http://glz.io

License:
    GPLv2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

=cut


## Standard includes
use strict;
use warnings;
use LockFile::Simple;
use Log::Handler;
use File::Basename;


## Gzb includes
use Gzb::Common;
use Gzb::Zfs;


# script options
my %options         = ();
$options{'version'} = 'master';
$options{'name'}    = (split(/\./, basename("$0")))[0];
$options{'debug'}   = 0;

$options{'logh'}    = undef;
$options{'log'}     = '/var/log/glz_zfs_backups/'.$options{'name'}.'.log';
$options{'lock'}    = '/var/run/'.$options{'name'};
$options{'common'}  = '/usr/local/etc/glz_zfs_backups/common.conf';
$options{'config'}  = '/usr/local/etc/glz_zfs_backups/'.$options{'name'}.'.conf';


# Check lock ?
my $lockMgr = LockFile::Simple->make(-format => '%f.lock', -stale => 1, -hold => 0);

if(not $lockMgr->trylock($options{'lock'})) {

    die($options{'name'}.' is already running !?!?');
}


# General log
%options = create_log_handler(\%options);
if(not defined($options{'logh'})) {

    die('Can not write log: '.$options{'log'});
}

# Parameters from file
my %parameters = read_config_files(\%options);


# Put a tag in general log
$options{'logh'}->info("Job begin");

# Go accross hashtable and do the jobs !
while(my($dataset, $toKeep) = each(%{$parameters{'clean'}}) ) {

	$options{'logh'}->info("Working on $dataset");

	my $snapshotCounter=0;
	# remove old snapshots
	if(check_if_dataset_exist(\%options, \%parameters, $dataset)) {

		write_debug_if_requested(\%options, "dataset: $dataset present");

		my @allSnapshots=get_all_snapshots(\%options, \%parameters, $dataset);

		foreach my $snapshot (@allSnapshots) {

			$snapshotCounter++;
            write_debug_if_requested(\%options, "Snapshot: $snapshot, number: $snapshotCounter");

			if($snapshotCounter > $toKeep) {

				$options{'logh'}->info("Need to delete snapshot: $snapshot");

				my $exitCode = destroy_snapshot(\%options, \%parameters, $snapshot);
				if($exitCode) {

					$options{'logh'}->info("Successfully delete snapshot $snapshot");
				}
				else {

					$options{'logh'}->error("Problem happened during delete snapshot: $snapshot");
				}
			}
			else {

                write_debug_if_requested(\%options, "No need to delete snapshot: $snapshot");
			}
		}
	}
	else {

		$options{'logh'}->error("local dataset: $dataset does not exist !");
	}
}

# Put a tag in general log
$options{'logh'}->info("Job end");

# unlock
$lockMgr->unlock($options{'lock'});

